<?php

/**
 * @file
 * Drush commands for revision disintegrate.
 */

use Drupal\revision_disintegrate\RevisionTrial\RevisionTrialNode;
use Drupal\revision_disintegrate\RevisionExecutioner\RevisionExecutioner;

/**
 * Implements hook_drush_command().
 */
function revision_disintegrate_drush_command() {
  $items['revision-disintegrate-node'] = [
    'description' => "Delete superfluous revisions related to a single or several nodes.

In heavy editorial environments, nodes with new revisions set as default will
result in many very similar revisions. Over time as the database grows and the
value of these revisions diminish. This command will delete these 'lesser
valued' revisions by comparing revisions with a set of factors:
  - When was the previous revision saved
  - Who was the author of the revision
  - How much of the actual text differ, if the 'diff' module is enabled
  - Is it the first revision ever (nice for historical reasons.)",
    'arguments' => [
      'nid ..' => "Node ID or set of Node IDs.",
    ],
    'options' => [
      'content-type' => [
        'description' => "Delete all revisions in nodes of given content type(s). Types are separated by comma.",
        'value' => 'required',
        'example-value' => 'page,article',
      ],
      'all' => [
        'description' => "Delete all revisions in all nodes.",
      ],
    ],
    'aliases' => ['rdn'],
  ];
  return $items;
}

/**
 * Drush command callback for 'revision-disintegrate-node'.
 */
function drush_revision_disintegrate_node() {
  $candidate_nids = [];
  drush_print("Building candidate nids:", 0, NULL, FALSE);
  $arg_nids = drush_get_arguments();
  array_shift($arg_nids);
  if ($arg_nids) {
    $arg_i = 0;
    foreach ($arg_nids as $nid) {
      $candidate_nids[$nid] = $nid;
      $arg_i++;
    }
    drush_print(" Argument: {$arg_i}.", 0, NULL, FALSE);
  }
  if ($types = drush_get_option('content-type')) {
    foreach (explode(',', $types) as $type) {
      $efq = new EntityFieldQuery();
      $type_nids = $efq->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', $type)
        ->propertyOrderBy('nid')
        ->execute();
      $type_i = 0;
      foreach ($type_nids['node'] as $nid => $dummy) {
        $candidate_nids[$nid] = $nid;
        $type_i++;
      }
      drush_print(" {$type}: {$type_i}.", 0, NULL, FALSE);
    }
  }
  if (drush_get_option('all')) {
    $select = db_select('node')->fields('node', ['nid']);
    $select->orderBy('nid');
    $all_nids = $select->execute()->fetchCol();
    $all_i = 0;
    foreach ($all_nids as $nid) {
      $candidate_nids[$nid] = $nid;
      $all_i++;
    }
    drush_print(" All: {$all_i}", 0, NULL, FALSE);
  }
  drush_print(" Total: " . count($candidate_nids) . '.');
  revision_disintegrate_drush_purge_node_revisions($candidate_nids);
}

/**
 * Delete superfluous revisions in all given nids.
 *
 * @param int[] $nids
 *   Array of node ids.
 */
function revision_disintegrate_drush_purge_node_revisions(array $nids) {
  $nids_total = count($nids);
  $nid_count = 0;
  foreach ($nids as $nid) {
    $nid_count++;
    $percent = round(100.0 * $nid_count / $nids_total, 1);
    drupal_static_reset('revision_disintegrate_drush_trial_progress');
    drupal_static_reset('revision_disintegrate_drush_executioner_progress');
    // Disintegrating revisions for node/3573. (3189/4298 – 87.4%):
    drush_print("Disintegrating revisions for node/{$nid}. ({$nid_count}/{$nids_total} – {$percent}%):");
    $trial = new RevisionTrialNode('node', $nid);
    $trial->addTrialProgressCallback('revision_disintegrate_drush_trial_progress');

    drush_print('Running trial: [', 0, NULL, FALSE);
    $verdicts = $trial->getRevisionVerdicts();
    $guilty = $trial->getGuiltyRevisions();
    drush_print("] Setup for deletion: " . count($guilty) . " / " . count($verdicts));
    if (!count($guilty)) {
      continue;
    }

    $executioner = new RevisionExecutioner('node', $nid, $guilty);
    $executioner->addProgressCallback('revision_disintegrate_drush_executioner_progress');

    drush_print("Deleting revisions [", 0, NULL, FALSE);
    $executioner->execute();
    drush_print("] " . count($guilty) . " deleted.");
    $trial->cacheClear();
  }
}

/**
 * Revision trial progress callback.
 */
function revision_disintegrate_drush_trial_progress(RevisionTrialNode $trial, $revision_id, $verdict, $count) {
  $total = $trial->getRevisionCount();
  revision_disintegrate_drush_progress_bar_progress(__FUNCTION__, $count, $total);
}

/**
 * Revision executioner progress callback.
 */
function revision_disintegrate_drush_executioner_progress(RevisionExecutioner $executioner, $revision_id, $count) {
  $total = count($executioner->getVictims());
  revision_disintegrate_drush_progress_bar_progress(__FUNCTION__, $count, $total);
}

/**
 * Primitive console progress bar.
 *
 * @param string $key
 *   The ID of this progress bar (Used by drupal_static()).
 * @param int $current
 *   The current position.
 * @param int $total
 *   The total amount of items during progress.
 * @param int $width
 *   The width of the progress bar (dots).
 */
function revision_disintegrate_drush_progress_bar_progress($key, $current, $total, $width = 60) {
  $current_char_pos = &drupal_static($key, 0);

  $char_pos = $width * $current / $total;

  while ($char_pos > $current_char_pos) {
    drush_print('.', 0, NULL, FALSE);
    $current_char_pos++;
  }
}
