<?php

namespace Drupal\revision_disintegrate\JuryMember;

use Drupal\revision_disintegrate\JuryMember\Diff\EntityDiffStatistics;

/**
 * Revision disintegration jury member for diff.module.
 */
class JuryMemberDiff extends JuryMemberBase {

  /**
   * The number of words we think deserve a new revision, no matter what.
   */
  const MAX_WORDS = 200;

  /**
   * Factor of total number of words to consider edited words against.
   */
  const MAX_WORDS_FACTOR = 0.15;

  /**
   * The number of added or removed lines that deserves a new revision.
   */
  const MAX_LINES = 30;

  /**
   * Part of total number of lines to consider added or deleted lines against.
   */
  const MAX_LINES_FACTOR = 0.2;

  /**
   * {@inheritdoc}
   */
  public function vote($suspect, $compare) {
    $diffStats = new EntityDiffStatistics($this->entityType, $suspect->{$this->revisionKey}, $compare->{$this->revisionKey});
    if (!$diffStats->differ()) {
      // We feel pretty strong about this. If there is absolutely no change
      // what-so-ever, there is really no need at all to keep this revision.
      return 1.0;
    }
    $stats = $diffStats->getStats();
    // Our vote works internally much like the overall voting mechanism in the
    // RevisionTrial setup. Each member has a vote. Our diff jury member has
    // internally four votes: One for the number of words edited alone with max
    // limit, one vote for number of words in relation to max number of words,
    // one vote for the number of lines *added* or *deleted* and one vote for
    // the number of lines it hampered with in relation to the total number of
    // lines.
    //
    // This is the words alone vote.
    $words = max($stats['words added'], $stats['words deleted']);
    $words_vote = 1.0 - ($words > self::MAX_WORDS ? 1.0 : ($words / self::MAX_WORDS));

    // One vote for the number of words in relation to words in total, with
    // upper limit of MAX_WORDS_FACTOR.
    $words_orig = $stats['words orig'] > 0 ? $stats['words orig'] : 1;
    $word_rel_vote = 1.0 - ($words / ($words_orig * self::MAX_WORDS_FACTOR));

    if ($word_rel_vote < 0.0) {
      $word_rel_vote = 0.0;
    }

    // Lines alone.
    $lines = max($stats['lines added'], $stats['lines deleted']) + $stats['lines changed'];
    $lines_vote = 1.0 - ($lines > self::MAX_LINES ? 1.0 : ($lines / self::MAX_LINES));

    // Edited lines compared with total number of lines.
    $lines_orig = $stats['lines orig'] > 0 ? $stats['lines orig'] : 1;
    $lines_rel_vote = 1.0 - ($lines / ($lines_orig * self::MAX_LINES_FACTOR));
    if ($lines_rel_vote < 0.0) {
      $lines_rel_vote = 0.0;
    }

    // Divide by number of internal votes.
    return ($words_vote + $word_rel_vote + $lines_vote + $lines_rel_vote) / 4;
  }

}
