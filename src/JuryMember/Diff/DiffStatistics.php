<?php

namespace Drupal\revision_disintegrate\JuryMember\Diff;

/**
 * Report the number of edits in diffs.
 *
 * This calculates the total number of lines and words added, changed or deleted
 * in diffs, on a per run basis and a grand total.
 *
 * It has a very wide public api due to overriding \DiffFormatter and its old
 * school PHP implementation. This is, of course, not a Formatter, but the
 * Formatter API is quite handy for the purpose of counting statistics in diffs.
 */
class DiffStatistics extends \DiffFormatter {

  // @see \WordLevelDiff:MAX_LINE_LENGTH().
  const MAX_LINE_LENGTH = 10000;

  /**
   * Stats per diff addition.
   *
   * @var array
   */
  protected $statsRun = [
    'lines added' => 0,
    'lines deleted' => 0,
    'lines changed' => 0,
    'words added' => 0,
    'words deleted' => 0,
  ];

  /**
   * Stats in total for all additions.
   *
   * @var array
   */
  protected $statsTotal = [
    'lines added' => 0,
    'lines deleted' => 0,
    'lines changed' => 0,
    'words added' => 0,
    'words deleted' => 0,
  ];

  /**
   * Whether there *is* a difference in current diff.
   *
   * @var bool
   */
  protected $differLast = FALSE;

  /**
   * Wheter there is a difference in grand total.
   *
   * @var bool
   */
  protected $differTotal = FALSE;

  /**
   * Reset settings.
   */
  public function __construct() {
    $this->reset(TRUE);
  }

  /**
   * Add a new diff to the engine to include in its calculations.
   *
   * @return array
   *   An assocciated array with statistics for this addition, keyed by
   *   '(lines|words) (added|deleted|changed)' for lookup, or an empty array if
   *   there is no difference between parts.
   */
  public function addDiff($left, $right) {
    $left = is_array($left) ? $left : explode("\n", $left);
    $right = is_array($right) ? $right : explode("\n", $right);
    $diff = new \Diff($left, $right);
    // Call the formatter which in turn calls our implementation for
    // 'formatting' a diff.
    return $this->format($diff);
  }

  /**
   * Reset last run's statistics, or all stats if desired.
   *
   * @param bool $global
   *   Reset global stats if TRUE.
   */
  public function reset($global = FALSE) {
    $this->line_stats['counter']['x'] = 0;
    $this->line_stats['counter']['y'] = 0;
    $this->line_stats['offset']['x'] = 0;
    $this->line_stats['offset']['y'] = 0;

    $this->differLast = FALSE;
    $this->statsRun = [
      'lines added' => 0,
      'lines deleted' => 0,
      'lines changed' => 0,
      'words added' => 0,
      'words deleted' => 0,
    ];
    if ($global) {
      $this->differTotal = FALSE;
      $this->statsTotal = [
        'lines added' => 0,
        'lines deleted' => 0,
        'lines changed' => 0,
        'words added' => 0,
        'words deleted' => 0,
      ];
    }
  }

  /**
   * Return TRUE if there is an overall difference.
   */
  public function differ($last_addition_only = FALSE) {
    if ($last_addition_only) {
      return $this->differLast;
    }
    return $this->differTotal;
  }

  /**
   * Return current statistics for diff.
   */
  public function getStats() {
    return $this->statsTotal;
  }

  /**
   * Given an array of lines, count the total number of words.
   */
  public static function countWordsInArray($lines) {
    $count = 0;
    foreach ($lines as $line) {
      $count += self::countWords($line);
    }
    return $count;
  }

  /**
   * Count the number of words in given string.
   *
   * This uses the same regex as in \WordLevelDiff::_split().
   */
  public static function countWords($string) {
    if (drupal_strlen($string) > self::MAX_LINE_LENGTH) {
      return 1;
    }
    if (preg_match_all('/ ( [^\S\n]+ | [0-9_A-Za-z\x80-\xff]+ | . ) (?: (?!< \n) [^\S\n])? /xs', $string, $matches)) {
      return count($matches[0]);
    }
    return 1;
  }

  /*****************************************************************************
   *
   * \DiffFormat overrides.
   *
   ****************************************************************************/

  /**
   * These are required by the base class.
   */
  public $show_header = FALSE;
  public $line_stats = [];

  public function _start_diff() {
    $this->reset();
  }

  public function _end_diff() {
    foreach ($this->statsRun as $item => $value) {
      $this->statsTotal[$item] += $value;
    }
    if ($this->differLast) {
      $this->differTotal = TRUE;
      return $this->statsRun;
    }
    return [];
  }

  public function _block_header($xbeg, $xlen, $ybeg, $ylen) {}

  public function _start_block($header) {}

  public function _end_block() {}

  public function _lines($lines, $prefix = ' ') {}

  public function _context($lines) {}

  public function _added($lines) {
    $this->differLast = TRUE;
    $this->statsRun['lines added'] += count($lines);
    $this->statsRun['words added'] += $this->countWordsInArray($lines);
  }

  public function _deleted($lines) {
    $this->differLast = TRUE;
    $this->statsRun['lines deleted'] += count($lines);
    $this->statsRun['words deleted'] += $this->countWordsInArray($lines);
  }

  public function _changed($orig, $closing) {
    $this->differLast = TRUE;
    $lines_deleted = is_array($orig) ? count($orig) : 0;
    $lines_added = is_array($closing) ? count($closing) : 0;
    if ($lines_added !== $lines_deleted) {
      if ($lines_added > $lines_deleted) {
        $this->statsRun['lines added'] += ($lines_added - $lines_deleted);
      }
      else {
        $this->statsRun['lines deleted'] += ($lines_deleted - $lines_added);
      }
    }
    $this->statsRun['lines changed'] += min($lines_added, $lines_deleted);

    $diff = new \WordLevelDiff($orig, $closing);
    foreach ($diff->edits as $edit) {
      $words_deleted = is_array($edit->orig) ? count($edit->orig) : 0;
      $words_added = is_array($edit->closing) ? count($edit->closing) : 0;
      switch ($edit->type) {
        case 'add':
          $this->statsRun['words added'] += $words_added;
          break;

        case 'delete':
          $this->statsRun['words deleted'] += $words_deleted;
          break;

        case 'change':
          $this->statsRun['words added'] += $words_added;
          $this->statsRun['words deleted'] += $words_deleted;
          break;
      }
    }
  }

}
