<?php

namespace Drupal\revision_disintegrate\JuryMember\Diff;

/**
 * Get diff statistics about two entity revisions.
 */
class EntityDiffStatistics {

  /**
   * The diff statistics engine.
   *
   * @var DiffStatistics
   */
  protected $diffStats = NULL;

  protected $entityType;
  protected $revisionIdLeft;
  protected $revisionIdRight;

  /**
   * Entity revision diff statistics array.
   *
   * This is a decorated version of \DiffStatistics::getStats() with
   * additional information about total number of lines and words in original
   * (left) revision.
   *
   * @var array
   */
  protected $stats = [];

  /**
   * Hand over entity type and revisions to get statistics from.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $revision_left
   *   The entity revision ID comparison base.
   * @param int $revision_right
   *   The entity revision ID to compare with.
   */
  public function __construct($entity_type, $revision_left, $revision_right) {
    module_load_include('inc', 'diff', 'diff.pages');
    $this->entityType = $entity_type;
    $this->revisionIdLeft = $revision_left;
    $this->revisionIdRight = $revision_right;
  }

  /**
   * Bool comparison of the two revisions.
   *
   * @return bool
   *   TRUE if they differ.
   */
  public function differ() {
    $this->getStats();
    return $this->diffStats->differ();
  }

  /**
   * Get the numbers.
   */
  public function getStats() {
    if (!empty($this->diffStats)) {
      return $this->stats;
    }

    $this->diffStats = new DiffStatistics();
    $rev_left = entity_revision_load($this->entityType, $this->revisionIdLeft);
    $rev_right = entity_revision_load($this->entityType, $this->revisionIdRight);
    $entity_diffs = diff_compare_entities($rev_left, $rev_right, ['entity_type' => $this->entityType]);
    $lines_orig = 0;
    $words_orig = 0;
    foreach ($entity_diffs as $entity_diff) {
      list($left, $right) = diff_extract_state($entity_diff, 'raw');
      $lines_orig += count($left);
      $words_orig += DiffStatistics::countWordsInArray($left);
      $this->diffStats->addDiff($left, $right);
    }
    $this->stats = $this->diffStats->getStats();
    $this->stats['lines orig'] = $lines_orig;
    $this->stats['words orig'] = $words_orig;
    return $this->stats;
  }

}
