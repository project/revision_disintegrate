<?php

namespace Drupal\revision_disintegrate\JuryMember;

/**
 * Boilerplate code for revision jury members.
 */
abstract class JuryMemberBase implements JuryMemberInterface {
  protected $entity;
  protected $entityType;
  protected $idKey;
  protected $revisionKey;
  protected $revisionTable;

  /**
   * This jury member's weight.
   *
   * @var float
   */
  private $weight = 1.0;

  /**
   * Jury member setup.
   *
   * @param string $entity_type
   *   Entity type we're judge of.
   * @param object $entity
   *   The entity itself.
   */
  public function __construct($entity_type, $entity) {
    $this->entityType = $entity_type;
    $this->entity = $entity;
    $entity_info = entity_get_info($this->entityType);
    $this->idKey = $entity_info['entity keys']['id'];
    $this->revisionKey = $entity_info['entity keys']['revision'];
    $this->revisionTable = $entity_info['revision table'];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function vote($suspect, $compare);

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight = 1.0) {
    $this->weight = $weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

}
