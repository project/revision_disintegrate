<?php

namespace Drupal\revision_disintegrate\JuryMember;

/**
 * Timestamp jury member for two revisions.
 */
class JuryMemberTimestamp extends JuryMemberBase {

  /**
   * Factor used to compare two revisions relative to today.
   *
   * Older revisions have less value. This factor is used to calculate the
   * timespan difference when comparing two revisions, relative to today. A
   * factor of 100 results in a minimum difference of 3.65 days for one year old
   * revisions.
   */
  const NEARBY_FACTOR = 200.0;

  /**
   * Pivot (in seconds) for leaning towards keeping revisions.
   *
   * Bigger values leans towards innocent, smaller towards guilty.
   */
  const RELATIVE_MAX = 60.0 * 60.0 * 24.0 * 30.0 * 3;

  /**
   * Property to access the timestamp on the entity object.
   *
   * @var string
   */
  protected $timestampPropertyEntity;

  /**
   * Property to access the timestamp on the revision object.
   *
   * @var string
   */
  protected $timestampPropertyRevision;

  /**
   * Setup timestamp revision jury member.
   */
  public function __construct($entity_type, $entity, $timestamp_property_entity = 'revision_timestamp', $timestamp_property_revision = 'timestamp') {
    parent::__construct($entity_type, $entity);
    $this->timestampPropertyEntity = $timestamp_property_entity;
    $this->timestampPropertyRevision = $timestamp_property_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function vote($suspect, $compare) {
    $now = REQUEST_TIME;

    $compare_timestamp = (float) $compare->{$this->timestampPropertyRevision};
    $suspect_timestamp = (float) $suspect->{$this->timestampPropertyRevision};
    $entity_timestamp = (float) $this->entity->{$this->timestampPropertyEntity};

    // Revisions newer than current should preferrably be kept as is.
    if ($suspect_timestamp > $entity_timestamp) {
      return 0.0;
    }

    // On to the actual algorithm: Revisions have higher value the more recent
    // they are. When they come of age, 'nearby' revisions in time are less
    // valuable. It's also a key point to have revisions at a given max
    // interval, eg. one every quarter.
    //
    // Consider this figure:
    //
    //           <- $diff -> <-- $relative -->
    // Time: ———|———————————|—————————————————|
    //      $suspect    $compare            $now
    //
    // We have three checkpoints in time to consider. The suspect is always the
    // oldest of these. The shorter $diff is, the less value our suspect is
    // supposed to have. This is solved by dividing $diff with $relative. In
    // addition we multiply $diff with a factor to adjust the importance of
    // nearby revisions. The bigger the factor is, the more important nearby
    // revisions are. A neat trick here is to consider $diff and $relative as
    // tangents which gives us a end result between 0 and π/2 using the inverse
    // tangent atan(). The surrounding computations is just to map the end
    // result between the 0.0 – 1.0 range with 1.0 as guilty (remove).
    $diff = abs($compare_timestamp - $suspect_timestamp);
    $relative = abs($now - $compare_timestamp);
    if ($relative > self::RELATIVE_MAX) {
      $relative = self::RELATIVE_MAX;
    }
    return 1.0 - (2 * atan($diff * self::NEARBY_FACTOR / $relative) / M_PI);
  }

}
