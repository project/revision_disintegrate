<?php

namespace Drupal\revision_disintegrate\JuryMember;

/**
 * Simple jury member for bumping the importance of the first revision.
 */
class JuryMemberFirst extends JuryMemberBase {

  /**
   * This entity's first revision by ID.
   *
   * @var int
   */
  protected $firstRevisionId;

  /**
   * {@inheritdoc}
   */
  public function __construct($entity_type, $entity) {
    parent::__construct($entity_type, $entity);
    $this->firstRevisionId = db_select($this->revisionTable, 'revision')
      ->fields('revision', [$this->revisionKey])
      ->condition($this->idKey, $entity->{$this->idKey})
      ->orderBy($this->revisionKey, 'ASC')
      ->range(NULL, 1)
      ->execute()
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function vote($suspect, $compare) {
    if ($suspect->{$this->revisionKey} == $this->firstRevisionId) {
      return 0.0;
    }
    return NULL;
  }

}
