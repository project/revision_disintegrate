<?php

namespace Drupal\revision_disintegrate\JuryMember;

/**
 * Author (user id) jury member for revisions.
 */
class JuryMemberAuthor extends JuryMemberBase {
  /**
   * Property to access the author on the entity object.
   *
   * @var string
   */
  protected $authorPropertyEntity;

  /**
   * Property to access the author on the revision object.
   *
   * @var string
   */
  protected $authorPropertyRevision;

  /**
   * {@inheritdoc}
   */
  public function __construct($entity_type, $entity, $author_property_revision = 'uid', $author_property_entity = 'uid') {
    parent::__construct($entity_type, $entity);
    $this->authorPropertyRevision = $author_property_revision;
    $this->authorPropertyEntity = $author_property_entity;
  }

  /**
   * {@inheritdoc}
   */
  public function vote($suspect, $compare) {
    // Rationale for values: If another user has edited *anything* we should
    // strongly oppose execution of it, though not completely. If the revision
    // is edited by the same user, we're more neutral and leave it up to the
    // other members to decide, even though we weight a little towards killing
    // it.
    return $suspect->{$this->authorPropertyRevision} == $compare->{$this->authorPropertyRevision} ? 0.6 : 0.1;
  }

}
