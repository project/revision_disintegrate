<?php

namespace Drupal\revision_disintegrate\JuryMember;

/**
 * API for revision judges.
 */
interface JuryMemberInterface {

  /**
   * Determine the livelihood of target revision.
   *
   * @param object $target
   *   The revision to test.
   * @param object $compare
   *   Revision for comparison.
   *
   * @return float|null
   *   A factor between 0.0 and 1.0 where 0.0 is innocent (keep) and 1.0 is
   *   guilty (kill, delete, execute). If the jury member choose not to
   *   participate in the voting for a given revision, it can simply return
   *   NULL.
   *
   *   Be vary about giving clear 0.0 and 1.0 values. In cases where only two
   *   jury members operates the companion member will struggle to have
   *   *anything* to say if the vote is pure black/white from this.
   */
  public function vote($target, $compare);

  /**
   * Set the weight of this jury member's vote.
   *
   * The RevisionTrial object conducts what weight the individual jury members
   * should have, and setting and getting this weight should be honored. This
   * value is trusted to the individual jury members for special cases where it
   * makes sense that its vote weights more or less than others.
   *
   * @param float $weight
   *   The weight as a factor.
   */
  public function setWeight($weight = 1.0);

  /**
   * Get the weight of this jury member's vote.
   *
   * @return float
   *   This jury member's weight. Default should be whatever was set by
   *   self::setWeight() or 1.0 if never set.
   */
  public function getWeight();

}
