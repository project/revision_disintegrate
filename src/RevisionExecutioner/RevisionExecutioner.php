<?php

namespace Drupal\revision_disintegrate\RevisionExecutioner;

/**
 * Execute (delete) revisions.
 */
class RevisionExecutioner {

  /**
   * The entity type to delete revisions from.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Entity ID.
   *
   * @var int
   */
  protected $entityId;

  /**
   * The list of victims setup for execution/deletion.
   *
   * @var array
   */
  protected $victims;

  /**
   * Callback post deletion of individual revisions.
   *
   * @var callable[]
   */
  protected $progressCallbacks = [];

  /**
   * Post delete callback.
   *
   * @var callable
   */
  protected $postCallback = NULL;

  /**
   * Constructor. Setup victims.
   */
  public function __construct($entity_type, $entity_id, $victims) {
    $this->entityType = $entity_type;
    $this->entityId = $entity_id;
    $this->victims = $victims;
  }

  /**
   * Delete revisions.
   */
  public function execute($process_batch = FALSE) {
    if (!drupal_is_cli() && count($this->victims) > 10) {
      $class_name = get_class($this);
      $batch = [
        'title' => t('Deleting revisions'),
        'operations' => [
          [
            "{$class_name}::batchExecute",
            [$this->entityType, $this->entityId, $this->victims, $this->postCallback],
          ],
        ],
        'finished' => "{$class_name}::batchFinished",
      ];
      batch_set($batch);
      if ($process_batch) {
        batch_process(is_string($process_batch) ? $process_batch : NULL);
      }
    }
    else {
      $deleted_count = 0;
      foreach ($this->victims as $revision_id) {
        entity_revision_delete($this->entityType, $revision_id);
        $deleted_count++;
        foreach ($this->progressCallbacks as $callback) {
          $callback($this, $revision_id, $deleted_count);
        }
      }
      drupal_set_message(t("Successfully deleted @count revisions", ['@count' => count($this->victims)]));
      if ($this->postCallback) {
        $post_callback = $this->postCallback;
        $post_callback($this->entityType, $this->entityId, $this->victims);
      }
    }
  }

  /**
   * Getter for our victims.
   */
  public function getVictims() {
    return $this->victims;
  }

  /**
   * Add a callback to be executed during (post) deletion of revisions.
   *
   * @param callable $callback
   *   The callback to be executed.
   */
  public function addProgressCallback(callable $callback) {
    if (is_callable($callback)) {
      $this->progressCallbacks[] = $callback;
    }
  }

  /**
   * Setter for $postCallback.
   */
  public function setPostCallback(callback $callback) {
    if (is_callable($callback)) {
      $this->postCallback = $callback;
    }
  }

  /**
   * Batch API static progress callback.
   */
  public static function batchExecute($entity_type, $entity_id, $victims, $finished_callback, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox'] = [
        'victims' => $victims,
        'progress' => 0,
        'max' => count($victims),
      ];
    }
    if (empty($context['results'])) {
      $context['results'] = [
        'deleted' => [],
        'finished_callback' => $finished_callback,
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
      ];
    }

    for ($i = 0; !empty($context['sandbox']['victims']) && $i < 10; $i++) {
      $revision_id = array_shift($context['sandbox']['victims']);
      entity_revision_delete($entity_type, $revision_id);
      $context['sandbox']['progress']++;
      $context['results']['deleted'][] = $revision_id;
    }
    $context['message'] = t("@current of @max revisions deleted.", ['@current' => $context['sandbox']['progress'], '@max' => $context['sandbox']['max']]);
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

  /**
   * Batch API static finished callback.
   */
  public static function batchFinished($success, $results, $operations) {
    if ($success) {
      $message = t("Successfully deleted @count revisions", ['@count' => count($results['deleted'])]);
    }
    else {
      $message = t("An error occurred during revision deletion. See log for more information");
    }
    drupal_set_message($message);
    if (is_callable($results['finished_callback'])) {
      $results['finished_callback']($results['entity_type'], $results['entity_id'], $results['deleted']);
    }
  }

}
