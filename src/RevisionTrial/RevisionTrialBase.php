<?php

namespace Drupal\revision_disintegrate\RevisionTrial;

/**
 * Boilerplate revision trial code.
 */
abstract class RevisionTrialBase implements RevisionTrialInterface {

  /**
   * The tipping points where we consider a revision guilty.
   */
  const GUILTY_PIVOT = 0.5;

  /**
   * The (revisionable) entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Entity ID.
   *
   * @var int
   */
  protected $entityId;

  /**
   * The actual entity.
   *
   * @var object
   */
  protected $entity;

  /**
   * Name of the entity's ID field in the entity database table.
   *
   * @var string
   */
  protected $idKey;

  /**
   * Name of entity's revision database table field.
   *
   * @var string
   */
  protected $revisionKey;

  /**
   * The revision table for this entity type.
   *
   * @var string
   */
  protected $revisionTable;

  /**
   * Current active revision.
   *
   * @var int
   */
  protected $currentRevision;

  /**
   * The available revisions.
   *
   * @var object[]
   */
  protected $revisions = NULL;

  /**
   * The number of revisions of this entity.
   *
   * @var int
   */
  protected $revisionCount = 0;

  /**
   * Callbacks to call during trials.
   *
   * @var callable[]
   */
  protected $trialProgressCallbacks = [];

  /**
   * Our jury.
   *
   * @var JuryMemberInterface[]
   */
  protected $jury = [];

  /**
   * Post trial results.
   *
   * @var object[]
   */
  protected $verdicts = [];

  /**
   * Set up stuff.
   */
  public function __construct($entity_type, $entity_id) {
    $this->entityType = $entity_type;
    $this->entityId = $entity_id;
    $this->entity = entity_load_single($entity_type, $entity_id);
    $this->entityInfo = entity_get_info($entity_type);
    $this->idKey = $this->entityInfo['entity keys']['id'];

    if (empty($this->entityInfo['entity keys']['revision'])) {
      throw new Exception("Cannot instantiate " . __CLASS__ . ": Entity type not revisionable: " . $entity_type);
    }
    $this->revisionKey = $this->entityInfo['entity keys']['revision'];
    $this->revisionTable = $this->entityInfo['revision table'];
    $this->currentRevision = $this->entity->{$this->revisionKey};
  }

  /**
   * Alter the jury.
   *
   * This must be called by child implementations of this class after the jury
   * has been setup, preferrably in the constructor.
   */
  protected function juryAlter() {
    drupal_alter('revision_disintegrate_jury', $this->jury);
  }

  /**
   * {@inheritdoc}
   */
  public function query($start = 0, $length = 0) {
    $query = db_select($this->revisionTable, 'revision');
    $query->addField('revision', $this->revisionKey);
    $query->condition($this->idKey, $this->entityId);
    if ($length) {
      $query->range(0, $length);
    }
    return $query;
  }

  /**
   * Return the number of revisions that this entity has.
   */
  public function getRevisionCount() {
    if (!$this->revisionCount) {
      $this->revisionCount = db_query("select count({$this->revisionKey}) from {{$this->revisionTable}} where {$this->idKey} = {$this->entityId}")->fetchField();
    }
    return $this->revisionCount;
  }

  /**
   * {@inheritdoc}
   */
  public function verdict($suspect, $compare) {
    $sum = 0.0;
    $participants = 0;
    $suspect->votes = [];
    foreach ($this->jury as $member_name => $member) {
      $vote = $member->vote($suspect, $compare);
      if (!is_numeric($vote)) {
        continue;
      }
      if ($vote < 0.0 || $vote > 1.0) {
        throw new \RangeException("Jury member votes must be between 0.0 and 1.0. Got {$vote} from {$member_name}.");
      }
      $suspect->votes[$member_name] = $vote;
      $vote *= $member->getWeight();
      $participants += $member->getWeight();
      $sum += $vote;
    }
    return $sum / $participants;
  }

  /**
   * Execute trials on all or a selected set of revisions.
   *
   * @param mixed $start
   *   At what position in the revision table should we start. This value
   *   is passed on to our (entity overridden) query() method. As such its type
   *   varies. Maybe it's timestamp based, maybe it's revision key.
   * @param int $length
   *   The number of items to evaluate.
   * @param object $previous
   *   The revision used for comparison. If empty, the frist revision is used as
   *   base.
   *
   * @return object[]
   *   An array of revision objects as setup by the query() method. In addition
   *   the individual verdict is added to the revision objects. This is also
   *   stored in $this->verdicts.
   */
  protected function runTrial($start = 0, $length = 0, &$previous = NULL) {
    $this->verdicts = [];
    $query = $this->query($start, $length);
    $result = $query->execute();
    $trial_count = 0;
    while ($revision = $result->fetchObject()) {
      if (!$previous) {
        $previous = $revision;
        // Assuming this is the latest edited revision, don't mess with it.
        $previous->revisionVerdict = 0.0;
      }
      // Don't mess with the current revision.
      elseif ($revision->{$this->revisionKey} == $this->currentRevision) {
        $revision->revisionVerdict = 0.0;
      }
      else {
        $revision->revisionVerdict = $this->verdict($revision, $previous);
      }
      if ($revision->revisionVerdict < self::GUILTY_PIVOT) {
        $previous = $revision;
      }
      $this->verdicts[$revision->{$this->revisionKey}] = $revision;
      $trial_count++;
      foreach ($this->trialProgressCallbacks as $callback) {
        $callback($this, $revision->{$this->revisionKey}, $revision->revisionVerdict, $trial_count);
      }
    }
    return $this->verdicts;
  }

  /**
   * Add a callable to be executed during trials (post individual trial).
   *
   * @param callable $callback
   *   A PHP callable function, method or lambda.
   */
  public function addTrialProgressCallback(callable $callback) {
    if (is_callable($callback)) {
      $this->trialProgressCallbacks[$callback] = $callback;
    }
  }

  /**
   * Get the verdicts for revisions.
   *
   * Perform the individual existential verdict for all revisions.
   *
   * @param bool $reset
   *   If TRUE, don't retrieve verdicts from cache.
   *
   * @return array
   *   Assocciated array with revision ID as key and the verdict as value.
   */
  public function getRevisionVerdicts($reset = FALSE) {
    if (!$reset) {
      if ($cache = cache_get($this->getCacheId())) {
        $this->verdicts = $cache->data;
        return $this->verdicts;
      }
    }
    else {
      $this->cacheClear();
      $this->verdicts = [];
    }
    if (!drupal_is_cli() && $this->getRevisionCount() > 20) {
      // Revisions may come by the thousands in aggressive editorial
      // environments.
      $class_name = get_class($this);
      $batch = [
        'title' => t("Comparing revisions for disintegration"),
        'operations' => [
          [
            "{$class_name}::batchTrial", [
              $this->entityType,
              $this->entityId,
              $class_name,
            ],
          ],
        ],
        'finished' => "{$class_name}::batchTrialFinished",
      ];
      batch_set($batch);
      batch_process();
    }
    else {
      $this->runTrial();
    }
    $cached_verdicts = $this->verdictsCachePrepared();
    cache_set($this->getCacheId(), $cached_verdicts);
    return $cached_verdicts;
  }

  /**
   * Get all revisions set up for deletion (guilty).
   */
  public function getGuiltyRevisions() {
    $verdicts = $this->getRevisionVerdicts();
    $guilty = [];
    foreach ($verdicts as $revision_id => $verdict) {
      if ($verdict > self::GUILTY_PIVOT) {
        $guilty[$revision_id] = $revision_id;
      }
    }
    return $guilty;
  }

  /**
   * Get the value used for comparison.
   */
  abstract protected function getBatchComparisonValue($revision);

  /**
   * Batch API processing callback.
   */
  public static function batchTrial($entity_type, $entity_id, $trial_class_name, &$context) {
    $trial = new $trial_class_name($entity_type, $entity_id);
    if (empty($context['sandbox'])) {
      $context['sandbox'] = [
        'progress' => 0,
        'max' => $trial->getRevisionCount(),
        'start' => 0,
        'previous' => NULL,
        'innocent' => 0,
        'guilty' => 0,
      ];
    }

    if (empty($context['results'])) {
      $context['results'] = [
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
        'verdicts' => [],
      ];
    }
    $verdicts = $trial->runTrial($context['sandbox']['start'], 20, $context['sandbox']['previous']);
    foreach ($verdicts as $revision_id => $revision) {
      $context['results']['verdicts'][$revision_id] = $revision->revisionVerdict;
      if ($revision->revisionVerdict > self::GUILTY_PIVOT) {
        $context['sandbox']['guilty']++;
      }
      else {
        $context['sandbox']['innocent']++;
      }
    }
    $bookmark = end($verdicts);
    $context['sandbox']['progress'] += count($verdicts);
    $context['sandbox']['start'] = $trial->getBatchComparisonValue($bookmark);
    $context['message'] = "Processed {$context['sandbox']['progress']} / {$context['sandbox']['max']}. Keep: {$context['sandbox']['innocent']}. Setup for deletion: {$context['sandbox']['guilty']}";
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

  /**
   * Batch API finished callback.
   */
  public static function batchTrialFinished($success, $results, $operations) {
    if ($success) {
      cache_set(self::getCacheIdStatic($results['entity_type'], $results['entity_id']), $results['verdicts']);
    }
  }

  /**
   * Cleanup cache.
   */
  public function cacheClear() {
    cache_clear_all($this->getCacheId(), 'cache');
  }

  /**
   * Get the cache key for this entity.
   */
  public function getCacheId() {
    return self::getCacheIdStatic($this->entityType, $this->entityId);
  }

  /**
   * Get the cache key for a given entity.
   */
  public static function getCacheIdStatic($entity_type, $entity_id) {
    return 'rev-dis-' . $entity_type . '-' . $entity_id;
  }

  /**
   * Convert verdicts to cache storable.
   */
  private function verdictsCachePrepared() {
    $cache = [];
    foreach ($this->verdicts as $revision_id => $revision) {
      $cache[$revision_id] = $revision->revisionVerdict;
    }
    return $cache;
  }

}
