<?php

namespace Drupal\revision_disintegrate\RevisionTrial;

use Drupal\revision_disintegrate\JuryMember\JuryMemberAuthor;
use Drupal\revision_disintegrate\JuryMember\JuryMemberFirst;
use Drupal\revision_disintegrate\JuryMember\JuryMemberTimestamp;
use Drupal\revision_disintegrate\JuryMember\JuryMemberDiff;

/**
 * Node specific revision trial.
 */
class RevisionTrialNode extends RevisionTrialBase {

  /**
   * Gather jury members for node revision trials.
   */
  public function __construct($entity_type, $entity_id) {
    parent::__construct($entity_type, $entity_id);

    $this->jury['author'] = new JuryMemberAuthor($entity_type, $this->entity);
    $this->jury['first'] = new JuryMemberFirst($entity_type, $this->entity);
    $this->jury['timestamp'] = new JuryMemberTimestamp($entity_type, $this->entity);
    if (module_exists('diff')) {
      $this->jury['diff'] = new JuryMemberDiff($entity_type, $this->entity);
    }
    // The importance of the 'timestamp' jury member should be toned down. It
    // has far too much to say in old/recent revisions where diff and author has
    // something else to say.
    $this->jury['timestamp']->setWeight(0.5);

    // This 'first' jury member only has a say about the first revision. The
    // first revision is sort of a historic happening, so its importance should
    // be increased. Bump the weight of it to something near impossible to
    // override, but not completely.
    $this->jury['first']->setWeight(count($this->jury) - 2);
    parent::juryAlter();
  }

  /**
   * {@inheritdoc}
   */
  public function query($start = 0, $length = 0) {
    $query = parent::query($start, $length);
    $query->addField('revision', 'uid');
    $query->addField('revision', 'timestamp');
    $query->orderBy('timestamp', 'DESC');
    if ($start) {
      $query->condition('timestamp', $start, '<');
    }
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBatchComparisonValue($revision) {
    return $revision->timestamp;
  }

}
