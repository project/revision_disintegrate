<?php

namespace Drupal\revision_disintegrate\RevisionTrial;

/**
 * Interface for determining the fate of entity revisions.
 *
 * The trial consist of a set of jury members with individual verdicts and a
 * "judge" that determine the final sentence based on these verdicts.
 */
interface RevisionTrialInterface {

  /**
   * Retrieve the SelectQuery used to gather revisions.
   *
   * Build the SelectQuery used to retrieve necessary fields utilized by jury
   * members as object properties. It's therefore child imlementation's task to
   * add the necessary fields and provide a sane order of revisions.
   *
   * @return SelectQuery
   *   The SelectQuery object ready for execution.
   */
  public function query();

  /**
   * Determine the final verdict for a revision.
   *
   * @param object $suspect
   *   Entity revision that should receive a verdict.
   * @param object $compare
   *   The previosly (newer) revision that is used for comparison.
   *
   * @return float
   *   A value between 0.0 (innocent, keep) and 1.0 (guilty, delete).
   */
  public function verdict($suspect, $compare);

}
