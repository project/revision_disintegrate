Delete entity (node) revisions, but keep "important" ones.

In heavy editorial environments, revisionable entities may end up
having many very similar revisions. Over time as the database grows and the
value of these revisions diminish. This command will delete these 'lesser
valued' revisions by comparing revisions with a set of factors:

- When was the previous revision saved
- Who was the author of the revision
- How much of the actual text differ, if the 'diff' module is enabled
- Is it the first revision ever (nice for historical reasons.

Think of these factors as jury members and the mean value of all
individual verdict will determine the fate of each revision.

This is a site/content maintenance module and is ment to be used using
drush. But to get a *feel* about what revisions will be kept or
deleted, the revisions tab on node entities will be populated with the
individual verdict where green rows means it's kept and red rows are
set up for execution.
